package Przemyslaw1988.library;

/**
 * Book representation
 * @author Przemyslaw
 */
public class Book {

    // Book title
    private String title;
    // Book author
    private String author;
    // Book year production
    private int addYear;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getAddYear() {
        return addYear;
    }

    public void setAddYear(int addYear) {
        this.addYear = addYear;
    }
}